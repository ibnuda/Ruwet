module Constants where

import           Lib.Prelude

import           Control.Lens
import           Data.Graph.Inductive

import           Types

bus1, bus2, bus3, bus4, bus5, bus6, bus7, bus8 :: Bus
bus1 = Bus "Rosalia Indah 01"
bus2 = Bus "Rosalia Indah 02"
bus3 = Bus "Sumber Kencono 01"
bus4 = Bus "Sumber Kencono 02"
bus5 = Bus "Budiman 01"
bus6 = Bus "Budiman 02"
bus7 = Bus "Aneka Remaja 01"
bus8 = Bus "Aneka Remaja 02"

terminal1, terminal2, terminal3, terminal4, terminal5 :: Terminal
terminal1 = Terminal 1 "Tirtonadi" 5 [bus1]
terminal2 = Terminal 2 "Kampung Rambutan" 5 []
terminal3 = Terminal 3 "Kalideres" 5 [bus5, bus6]
terminal4 = Terminal 4 "Bungurasih" 5 [bus8]
terminal5 = Terminal 5 "Ciceheum" 4 [bus7]

route1, route2, route3, route4, route5, route6, route7, route8 :: Route
route1 = Route 1 "Solo - Jakarta (Rambutan)" [bus2]
route2 = Route 2 "Solo - Jakarta (Deres)" []
route3 = Route 3 "Solo - Surabaya" [bus3]
route4 = Route 4 "Surabaya - Solo" [bus4]
route5 = Route 5 "Jakarta - Surabaya" []
route6 = Route 6 "Jakarta - Solo" []
route7 = Route 7 "Jakarta - Bandung" []
route8 = Route 8 "Bandung - Jakarta" []

routeEdge1, routeEdge2, routeEdge3, routeEdge4, routeEdge5, routeEdge6, routeEdge7, routeEdge8 ::
     RouteEdge
routeEdge1 = RouteEdge 1 2 route1
routeEdge2 = RouteEdge 1 3 route2
routeEdge3 = RouteEdge 1 4 route3
routeEdge4 = RouteEdge 4 1 route4
routeEdge5 = RouteEdge 2 4 route5
routeEdge6 = RouteEdge 2 1 route6
routeEdge7 = RouteEdge 3 5 route7
routeEdge8 = RouteEdge 5 3 route8

graphOfTransportation :: Gr Terminal Route
graphOfTransportation =
  mkGraph
    (zip [1 ..] [terminal1, terminal2, terminal3, terminal4, terminal5])
    (map
       (\r -> (r ^. routeEdgeFrom, r ^. routeEdgeTo, r ^. routeEdgeRoute))
       [ routeEdge1
       , routeEdge2
       , routeEdge3
       , routeEdge4
       , routeEdge5
       , routeEdge6
       , routeEdge7
       , routeEdge8
       ])
