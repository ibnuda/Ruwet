{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE TemplateHaskell #-}
module Types where

import           Lib.Prelude

import           Control.Lens

import           Util

data Bus = Bus
  { _busName :: Text
  } deriving (Show, Eq)

data Terminal = Terminal
  { _terminalId:: Int,
    _terminalName     :: Text
  , _terminalCapacity :: Int
  , _terminalBusses   :: [Bus]
  } deriving (Show, Eq)

data Route = Route
  { _routeId     :: Int
  , _routeName   :: Text
  , _routeBusses :: [Bus]
  } deriving (Show, Eq)

data RouteEdge = RouteEdge
  { _routeEdgeFrom  :: Int
  , _routeEdgeTo    :: Int
  , _routeEdgeRoute :: Route
  } deriving (Show, Eq)

data TerminalNode = TerminalNode
  { _terminalNodeNode     :: Int
  , _terminalNodeTerminal :: Terminal
  } deriving (Show, Eq)

data Transportation = Transportation
  { _transportationTerminals :: [TerminalNode]
  , _transportationRoutes    :: [RouteEdge]
  } deriving (Show)

instance Ord Route where
  compare r1 r2 = _routeId r1 `compare` _routeId r2

instance Num Route where
  (+) r1 r2 = r1 {_routeId = (_routeId r1) + (_routeId r2)}
  (-) r1 r2 = r1 {_routeId = (_routeId r1) - (_routeId r2)}
  (*) r1 r2 = r1 {_routeId = (_routeId r1) * (_routeId r2)}
  abs = abs
  signum = signum
  fromInteger x = Route (fromInteger x) "" []

instance Real Route where
  toRational = toRational . _routeId

makeLenses ''Bus
makeLenses ''Terminal
makeLenses ''Route
makeLenses ''RouteEdge
makeLenses ''TerminalNode
makeLenses ''Transportation

deriveJSONUnprefixLower ''Bus
deriveJSONUnprefixLower ''Terminal
deriveJSONUnprefixLower ''Route
deriveJSONUnprefixLower ''RouteEdge
deriveJSONUnprefixLower ''TerminalNode
deriveJSONUnprefixLower ''Transportation
