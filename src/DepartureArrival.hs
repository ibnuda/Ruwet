module DepartureArrival where

import           Lib.Prelude

import           Control.Lens
import           Data.Either.Combinators
import           Data.Graph.Inductive    hiding ((&))
import qualified Data.Text               as Text (intercalate)

import           Types
import           Util

getLRouteByRouteName ::
    Graph gr => Text -> gr a Route -> Either Text (LEdge Route)
getLRouteByRouteName routename graph =
  case filter (\(_, _, route) -> route ^. routeName == routename) (labEdges graph) of
    [route] -> Right route
    _       -> Left $ "There's no route named \"" <> routename <> "\"."

getLTerminalByName ::
     Graph gr
  => Text
  -> gr Terminal b
  -> Either Text (LNode Terminal)
getLTerminalByName terminalname graph =
  case filter
         (\(_, term) -> term ^. terminalName == terminalname)
         (labNodes graph) of
    [terminal] -> Right terminal
    _          -> Left $ "Terminal named \"" <> terminalname <> "\" doesn't exist."

getLTerminalByNode ::
     Graph gr => Node -> gr Terminal Route -> Either Text (LNode Terminal)
getLTerminalByNode n graph = do
  term <- maybeToRight "The node of the term doesn't exist." $ lab graph n
  return (n, term)

getBusInLTerminal :: Text -> LNode Terminal -> Either Text Bus
getBusInLTerminal busname lterm =
  case filter (\b -> b ^. busName == busname) (term ^. terminalBusses) of
    [bus] -> Right bus
    _     -> Left $ "There's no bus named \"" <> busname <> "\" in terminal \"" <> term ^. terminalName <> "\"."
  where
    (_, term) = lterm

getBusInLRoute :: Text -> LEdge Route -> Either Text Bus
getBusInLRoute busname lroute =
  case filter (\b -> b ^. busName == busname) (route ^. routeBusses) of
    [bus] -> Right bus
    _     -> Left $ "There's no bus named \"" <> busname <> "\" in route \"" <> route ^. routeName <> "\"."
  where
    (_, _, route) = lroute

removeBusTerminal :: Bus -> Terminal -> Terminal
removeBusTerminal bus terminal =
  terminal & terminalBusses .~ (filter (/= bus) (terminal ^. terminalBusses))
addBusTerminal :: Bus -> Terminal -> Terminal
addBusTerminal bus terminal =
  terminal & terminalBusses .~ (bus : terminal ^. terminalBusses)

removeBusRoute :: Bus -> Route -> Route
removeBusRoute bus route =
  route & routeBusses .~ (filter (/= bus) (route ^. routeBusses))
addBusRoute :: Bus -> Route -> Route
addBusRoute bus route = route & routeBusses .~ (bus : route ^. routeBusses)

lnToNT :: (LNode Terminal) -> TerminalNode
lnToNT ln =
  let (n, t) = ln
  in TerminalNode n t

ntToLN :: TerminalNode -> (LNode Terminal)
ntToLN nt = (nt ^. terminalNodeNode, nt ^. terminalNodeTerminal)

leToRE :: (LEdge Route) -> RouteEdge
leToRE le =
  let (s, d, r) = le
  in RouteEdge s d r

reToLE :: RouteEdge -> (LEdge Route)
reToLE r = (r ^. routeEdgeFrom, r ^. routeEdgeTo, r ^. routeEdgeRoute)

grToTr :: Graph gr => gr Terminal Route -> Transportation
grToTr gr =
  let ts = map lnToNT $ labNodes gr
      rs = map leToRE $ labEdges gr
  in Transportation ts rs

trToGr :: Graph gr => Transportation -> gr Terminal Route
trToGr tr =
  mkGraph (map ntToLN $ tr ^. transportationTerminals) (map reToLE $ tr ^. transportationRoutes)

updateTerminalTransportation :: Terminal -> Transportation -> Transportation
updateTerminalTransportation nterm tr =
  tr & transportationTerminals .~
  (map
     (\tnode ->
        if tnode ^. terminalNodeTerminal ^. terminalId == nterm ^. terminalId
          then tnode & terminalNodeTerminal .~ nterm
          else tnode)
     (tr ^. transportationTerminals))

updateRouteTransportation :: Route -> Transportation -> Transportation
updateRouteTransportation nroute tr =
  tr & transportationRoutes .~
  (map
     (\tedge ->
        if tedge ^. routeEdgeRoute ^. routeId == nroute ^. routeId
          then tedge & routeEdgeRoute .~ nroute
          else tedge)
     (tr ^. transportationRoutes))

updateLNodeInGraph ::
     DynGraph gr
  => LNode Terminal
  -> LNode Terminal
  -> gr Terminal Route
  -> Either Text (gr Terminal Route)
updateLNodeInGraph oldnode newnode graph = do
  let (ln, lterm) = oldnode
      (nn, nterm) = newnode
      oldtransport = grToTr graph
  _ <- maybeToRight "Old node not found." (lab graph ln)
  _ <- maybeToRight "New node not found." (lab graph nn)
  _ <-
    flip
      falseToLeft
      "There's no terminal like that."
      (lterm `elem` map (^. terminalNodeTerminal) (oldtransport ^. transportationTerminals))
  return $ trToGr $ updateTerminalTransportation nterm oldtransport

updateLEdgeInGraph ::
     DynGraph gr
  => LEdge Route
  -> LEdge Route
  -> gr Terminal Route
  -> Either Text (gr Terminal Route)
updateLEdgeInGraph oldedge newedge graph = do
  let (lsd, ldd, ledge) = oldedge
      (nsd, ndd, nedge) = newedge
      oldtransport = grToTr graph
  _ <- flip falseToLeft "There's no route like that." (hasLEdge graph oldedge)
  _ <- maybeToRight ("Node " <> show lsd <> " not found.") (lab graph lsd)
  _ <- maybeToRight ("Node " <> show ldd <> " not found.") (lab graph ldd)
  _ <- maybeToRight ("Node " <> show nsd <> " not found.") (lab graph nsd)
  _ <- maybeToRight ("Node " <> show ndd <> " not found.") (lab graph ndd)
  _ <-
    flip
      falseToLeft
      "There's no route like that."
      (ledge `elem` map (^. routeEdgeRoute) (oldtransport ^. transportationRoutes))
  return $ trToGr $ updateRouteTransportation nedge oldtransport

checkTravellingBus ::
     Graph gr => [(Node, Route)] -> gr Terminal Route -> Either Text Text
checkTravellingBus nodesandroutes graph = do
  case filter (\(_, r) -> not . null $ r ^. routeBusses) nodesandroutes of
    x:_ -> shittyFunc x graph
    _   -> Left "Okay."

shittyFunc :: Graph gr => (Node, Route) -> gr Terminal Route -> Either Text Text
shittyFunc (n, r) graph = do
  (_, term) <- getLTerminalByNode n graph
  let busses = r ^. routeBusses
      busses'name = Text.intercalate "," . map (^. busName) $ busses
      busAmount = length busses
  return $
    "There are " <> show busAmount <> " bus(es) that will arrive from terminal " <>
    term ^.
    terminalName <>
    ", namely: " <>
    busses'name <>
    "."

succNode :: Graph gr => gr a b -> Node
succNode = succ . maximum . nodes

succTermId :: Graph gr => gr Terminal b -> Int
succTermId = succ . maximum . map (\(_, term) -> term ^. terminalId) . labNodes

succRouteId :: Graph gr => gr a Route -> Int
succRouteId = succ . maximum . map (\(_, _, r) -> r ^. routeId) . labEdges

departBus ::
    DynGraph gr
 => Text -- ^ Bus' name.
 -> Text -- ^ Route's name.
 -> gr Terminal Route -- ^ Graph of transportation map.
 -> Either Text (gr Terminal Route)
departBus busname routename graph = do
  oldlroute <- getLRouteByRouteName routename graph
  let (ns, nd, route) = oldlroute
  oldlterm <- getLTerminalByNode ns graph
  let (nts, term) = oldlterm
  departingbus <- getBusInLTerminal busname oldlterm
  let newroute = addBusRoute departingbus route
      newterm = removeBusTerminal departingbus term
      newlterm = (nts, newterm)
      newlroute = (ns, nd, newroute)
  updateLEdgeInGraph oldlroute newlroute graph >>=
    updateLNodeInGraph oldlterm newlterm

arriveBus ::
    DynGraph gr
 => Text -- ^ Bus' name.
 -> Text -- ^ Route's name.
 -> gr Terminal Route -- ^ Graph of transportation map.
 -> Either Text (gr Terminal Route)
arriveBus busname routename graph = do
  oldlroute <- getLRouteByRouteName routename graph
  let (ns, nd, route) = oldlroute
  oldlterm <- getLTerminalByNode nd graph
  let (nts, term) = oldlterm
  arrivingbus <- getBusInLRoute busname oldlroute
  let newroute = removeBusRoute arrivingbus route
      newterm = addBusTerminal arrivingbus term
      newlterm = (nts, newterm)
      newlroute = (ns, nd, newroute)
  updateLEdgeInGraph oldlroute newlroute graph >>=
    updateLNodeInGraph oldlterm newlterm

openTerminal ::
     DynGraph gr
  => Text -- ^ Terminal name.
  -> Int -- ^ Terminal capacity.
  -> gr Terminal b -- ^ Graph.
  -> Either Text (gr Terminal b)
openTerminal terminalname capacity graph = do
  _ <-
    swapEither
      (mapRight
         (\(_, term) -> "Terminal \"" <> term ^. terminalName <> " \" already exist.")
         (getLTerminalByName terminalname graph))
  let nextnode = succNode graph
      nextid = succTermId graph
  return $ insNode (nextnode, Terminal nextid terminalname capacity []) graph

closeTerminal ::
     Graph gr
  => Text
  -> gr Terminal Route
  -> Either Text (gr Terminal Route)
closeTerminal terminalname graph = do
  (nterm, term) <- getLTerminalByName terminalname graph
  let routestothisterm = lpre graph nterm
  _ <- swapEither $ checkTravellingBus routestothisterm graph
  _ <-
    falseToLeft
      (null $ term ^. terminalBusses)
      ("Terminal \"" <> terminalname <> "\" is not empty.")
  return $ delNode nterm graph

openRoute ::
     DynGraph gr
  => Text -- ^ Route name.
  -> Text -- ^ Origin terminal name.
  -> Text -- ^ Destination terminal name.
  -> gr Terminal Route -- ^ Graph.
  -> Either Text (gr Terminal Route)
openRoute routename ortermname desttermname graph = do
  (norigin, _) <- getLTerminalByName ortermname graph
  (ndest, _) <- getLTerminalByName desttermname graph
  _ <-
    swapEither
      (mapRight
         (\(_, _, r) -> "Route named \"" <> r ^. routeName <> "\" already exists.")
         (getLRouteByRouteName routename graph))
  let nextid = succRouteId graph
      newroute = (norigin, ndest, Route nextid routename [])
  return $ insEdge newroute graph

closeRoute ::
     DynGraph gr
  => Text
  -> gr Terminal Route
  -> Either Text (gr Terminal Route)
closeRoute routename graph = do
  target@(norigin, ndest, route) <- getLRouteByRouteName routename graph
  _ <- getLTerminalByNode norigin graph
  _ <- getLTerminalByNode ndest graph
  case null (route ^. routeBusses) of
    True  -> return $ delLEdge target graph
    False -> Left $ "Route \"" <> routename <> "\" is not empty."
