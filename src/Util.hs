module Util where

import           Data.Aeson
import           Data.Aeson.TH
import           Data.Char           (toLower)
import qualified Data.Text           as T
import           Lib.Prelude

import           Language.Haskell.TH

unprefixLower :: [Char] -> Options
unprefixLower prefix =
  defaultOptions {fieldLabelModifier = uncapitalize . dropprefix prefix}

firstchartolower :: [Char] -> [Char]
firstchartolower []     = []
firstchartolower (c:cs) | c == '_' = firstchartolower cs
firstchartolower (c:cs) = toLower c : cs

uncapitalize :: [Char] -> [Char]
uncapitalize []     = []
uncapitalize (c:cs) | c == '_' = uncapitalize cs
uncapitalize (c:cs) = toLower c : cs

dropprefix :: [Char] -> [Char] -> [Char]
dropprefix = dropprefix' "dropprefix" identity

dropsuffix :: [Char] -> [Char] -> [Char]
dropsuffix prefix input =
  reverse $ dropprefix' "dropsuffix" reverse (reverse prefix) (reverse input)

dropprefix' :: [Char] -> ([Char] -> [Char]) -> [Char] -> [Char] -> [Char]
dropprefix' fnname strtrans prefix input = go prefix input
  where
    contextual :: [Char] -> [Char]
    contextual msg =
      fnname ++ ": " ++ msg ++ ". " ++ strtrans prefix ++ " " ++ strtrans input
    go pre [] =
      panic . T.pack . contextual $ "prefix leftover: " ++ strtrans pre
    go [] (c:cs) = c : cs
    go (p:prerest) (c:crest)
      | p == c = go prerest crest
      | c == '_' = go (p : prerest) (crest)
      | otherwise =
        panic . T.pack . contextual $
        "not equal: (" ++
        strtrans (p : prerest) ++ ") /= (" ++ strtrans (c : crest) ++ ")"

deriveJSONUnprefixLower :: Name -> Q [Dec]
deriveJSONUnprefixLower name =
  deriveJSON (unprefixLower . firstchartolower $ nameBase name) name

falseToLeft :: Bool -> a -> Either a a
falseToLeft b text =
  case b of
    True  -> Right text
    False -> Left text

trueToLeft :: Bool -> a -> Either a ()
trueToLeft b text =
  case b of
    True  -> Left text
    False -> Right ()

