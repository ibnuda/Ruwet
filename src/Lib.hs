module Lib where

import           Lib.Prelude

import           Data.Aeson.Encode.Pretty
import           Data.Graph.Inductive

import           Constants
import           Types

import           DepartureArrival

printGraph :: Gr Terminal Route -> IO ()
printGraph = putStrLn . encodePretty . grToTr

printEitherGraph :: Either Text (Gr Terminal Route) -> IO ()
printEitherGraph e =
  case e of
    Left l  -> putText l
    Right r -> printGraph r

someFunc :: IO ()
someFunc = do
  putText "Initial state."
  printGraph graphOfTransportation
  putText "Departing Bus1."
  let graphAfterDeparted =
        departBus
          "Rosalia Indah 01"
          "Solo - Jakarta (Rambutan)"
          graphOfTransportation
  printEitherGraph graphAfterDeparted
  putText "Arriving Bus1"
  let graphAfterArrived =
        arriveBus "Rosalia Indah 01" "Solo - Jakarta (Rambutan)" =<<
        graphAfterDeparted
  printEitherGraph graphAfterArrived
  let departToBandung =
        departBus "Budiman 01" "Jakarta - Bandung" =<< graphAfterArrived
  printEitherGraph departToBandung
  let arriveAtBandung =
        arriveBus "Budiman 01" "Jakarta - Bandung" =<< departToBandung
  printEitherGraph arriveAtBandung
  let arriveAtSurabaya =
        arriveBus "Sumber Kencono 01" "Solo - Surabaya" =<< arriveAtBandung
  printEitherGraph arriveAtSurabaya
  let arriveAtSolo =
        arriveBus "Sumber Kencono 02" "Surabaya - Solo" =<< arriveAtSurabaya
  printEitherGraph arriveAtSolo
